#include <iostream>
#include <fstream>
#include <vector>


using namespace std;

// na wejsciu podaje aktualna permutacje pi, liczbe zadan n, liczbe maszyn m, informacje o zadaniach **zadania.
void Sortowanie(int *pi, int n,int m, int **zadania )
{
    int *suma = new int[n];
    // w petli sumuje czasy na wszyskich masynach dla kazdego zadania
    for( int i=1;i<n;++i)
    {
        suma[i]=0;
        pi[i]=i;
        for(int y=1;y<m;++y)
            suma[i]+=zadania[i][y];
    }
    // nastepnie sortuje permutacje po powyzszej sumie
    for( int i = 1; i < n; i++ )
    {
        for( int j = 1; j < n-1; j++ )
        {
            if( suma[j] < suma[j+1] )
                {
                    swap( suma[j], suma[j+1] );
                    for(int l=1;l<m;++l)
                        swap(zadania[j][l],zadania[j+1][l]);
                }
        }
    }
}


// na wejsciu podaje
// i - pozycja na ktora chce wlozyc nowy element w permutacji pi
// licznik - indeks najwiekszego, jeszcze nie wykozystanego elementu
// n - liczba zadan
// pi - stara permutacja
// newpi - nowa permutacja, wieksza o jeden element od pi
void wloz(int i, int licznik, int n, int *pi, int *newpi)
{
    // inicjuje newpi identyczne jak pi
    for(int e=1;e<n;++e)
        newpi[e]=pi[e];
    // wkladam nowy element na i-ta pozycje
    newpi[i]=pi[licznik-1];
    // przepisuje reszte elementow
    for(int iter =i+1;iter<licznik;++iter)
        newpi[iter] = pi[iter-1];
}

int Cmax(int n, int m, int *pi, int **zadania)
{
    // implementacja C
    int** C = new int*[n];
    for (int i = 0; i < n; i++)
        C[i] = new int[m];

    // inicjuje C zerami
    for(int i=0;i<n;i++)
        for(int j=0;j<m;j++)
        {
            C[i][j]=0;
        }

    // wypelniam tablice C wedlug ustalonego wzoru
    for(int i=1;i<n;i++)
        for(int j=1;j<m;j++)
            C[i][j]=(max(C[i-1][j],C[i][j-1])+zadania[pi[i]][j]);

    // zwracam Cmax dla aktualnej liczby zadan i maszyn
    return C[n-1][m-1];
}

// rekurencyjny algorytm NEH
int rek(int *pi, int licznik, int n, int m, int **zadania)
{
    int *newpi = new int[n];
    int zap=1;
    int l_min=9999999;
    int local_min=999999;

    // dokladam kolejne zadanie do aktualnego rozwiazania
    ++licznik;

    // jezeli przekroczylem liczbe zadan -> koniec algorytmu
    if(licznik==n+1)
        {
            return  Cmax(n,m,pi,zadania);
        }

    // petla obliczajaca Cmax dla wlozenia zadania na kazda mozliwa pozycje, zapamietuje najlepsza pozycje
    for(int i=1; i<licznik;++i)
    {
        wloz(i, licznik, n, pi,newpi);
        local_min=Cmax(licznik,m,newpi,zadania);
        if(local_min<l_min)
        {
            zap=i;
            l_min=local_min;
        }
    }
    // umieszczam zadanie na najlepszej pozycji
    wloz(zap, licznik, n, pi, newpi);
    for(int i=1; i<n;++i)
        pi[i]=newpi[i];
    delete newpi;
    // zwracam rekurencyjne wywolanie funkcji
    return rek(pi, licznik, n, m, zadania);
}

int main()
{
    ifstream plik;
	string zmienna;

	int n,m,i,j;

	cout << "Podaj nazwe pliku wejsciowego:  ";
	cin >> zmienna;
	cout << endl;

	/* otwieramy plik */
	plik.open(zmienna);

	/* sprawdzamy czy plik sie otworzyl */
	if (!plik.good() == true)
	{
		cout << "Nie mozna otworzyc pliku\n";
		return -1;
	}

	/* pobieramy liczbe zadan */
	plik >> n;
	n+=1;

	/* pobieramy liczbe maszyn */
    plik >> m;
    m+=1;

    /* unikamy bledu dla n<=0 || m<=0 */
	if (n <= 0 || m<=0)
	{
		cout << "Bledny plik z danymi\n";
		cout << "Liczba maszyn = "<<n<<endl;
		cout << "Liczba zadan  = "<<m<<endl;
		return -1;
	}

    // implementacja pi
    int *pi = new int[n];

    // implementacja tablicy z danymi zadan
    int** zadania = new int*[n];
    for (i = 0; i < n; i++)
        zadania[i] = new int[m];

    // wczytanie danych o zadaniach
    for(i=1;i<n;i++)
        for(j=1;j<m;j++)
        {
            plik>>zadania[i][j];
        }

    Sortowanie(pi,n,m,zadania);
    cout<<"REK:  "<<rek(pi, 1, n, m, zadania)<<endl;

}
